import { ReactFragment, ReactPropTypes } from "react";

type ButtonProps = ReactFragment & {
  text?: string;
  onClick?: () => any;
};
export function Button(props: ButtonProps) {
  return (
    <button {...props} className="py-2 px-4 text-white bg-blue-400 rounded-md">
      {props.text}
    </button>
  );
}
