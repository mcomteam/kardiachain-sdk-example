### The Graph
https://thegraph.com/explorer/subgraph?id=0x9bde7bf4d5b13ef94373ced7c8ee0be59735a298-2&version=0x9bde7bf4d5b13ef94373ced7c8ee0be59735a298-2-0&view=Playground
### ETH Price
    {
        bundles(first: 5) {
            id
            ethPriceUSD
        }
    }

### TVL

    {
        factories {
            totalValueLockedUSD
        }
    }

### TVL Day Data

    query {
        uniswapDayDatas(
            first: 1000
            skip: 0
            where: { date_gt: 1619170975 }
            orderBy: date
            orderDirection: asc
        ) {
            id
            date
            volumeUSD
            tvlUSD
        }
    }
