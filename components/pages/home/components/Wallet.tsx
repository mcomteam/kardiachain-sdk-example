import { KardiaAccount } from "kardia-js-sdk";
import { useEffect, useState } from "react";
import { Button } from "../../../shared/Button";
import Web3 from "web3";
import { useHomeContext } from "../providers/HomeProvider";
export default function Wallet() {
  const [data, setData] = useState({});
  const { setWalletAddress } = useHomeContext();
  let [extensionData, setExtensionData] = useState<any>({});
  let web3: Web3;
  let kardiachain: any;
  if (typeof window != "undefined") {
    kardiachain = (window as any).kardiachain;
  }
  const isExtensionEnabled = () => {
    if (kardiachain) {
      web3 = new Web3(kardiachain);
      if (kardiachain.isKaiWallet) {
        kardiachain.request({ method: "eth_requestAccounts" });
        return true;
      }
    }
    return false;
  };
  useEffect(() => {
    if (!isExtensionEnabled()) {
      setExtensionData({
        ...extensionData,
        extension: "KardiaChain Wallet Extension not found",
      });
    } else {
      const isKaiWallet = kardiachain.isKaiWallet;
      const isConnected = kardiachain.isConnected();
      kardiachain
        .request({ method: "eth_requestAccounts" })
        .then((accounts: string[]) => {
          extensionData = {
            ...extensionData,
            extension: "KardiaChain Wallet Extension Enalbed",
            isConnected,
            isKaiWallet,
            accounts,
          };
          setExtensionData(extensionData);
          setWalletAddress!(kardiachain.selectedAddress);
        });
      kardiachain.on("accountsChanged", (accounts: string[]) => {
        extensionData = { ...extensionData, accounts };
        setExtensionData(extensionData);
        setWalletAddress!(kardiachain.selectedAddress);
      });
      kardiachain.on("disconnect", (error: Error) => {
        extensionData = {
          ...extensionData,
          extension: error.message,
          isConnected: kardiachain.isConnected(),
        };
        setExtensionData(extensionData);
      });
      kardiachain.on("chainChanged", (chainId: string) => {
        console.log("chainChanged", chainId);
        extensionData = {
          ...extensionData,
          chainId: chainId,
        };
        setExtensionData(extensionData);
      });
      kardiachain.on("networkChanged", (networkId: string) => {
        console.log("networkChanged", networkId);
        extensionData = {
          ...extensionData,
          networkId: networkId,
        };
        setExtensionData(extensionData);
      });
      interface ProviderMessage {
        type: string;
        data: unknown;
      }
      kardiachain.on("message", (message: ProviderMessage) => {
        console.log("message", message);
        const messages = extensionData.messages || [];
        messages.push(message);
        extensionData = { ...extensionData, messages };
        setExtensionData(extensionData);
      });
    }
  }, []);
  const generateWallet = () => {
    const wallet = KardiaAccount.generateWallet();
    setData(wallet);
  };
  return (
    <>
      <h1>Wallet</h1>
      <Button text="Generate Wallet" onClick={generateWallet} />
      <pre>{JSON.stringify(data, null, 2)}</pre>
      <h1>Extension</h1>
      <pre>{JSON.stringify(extensionData, null, 2)}</pre>
    </>
  );
}
