UniswapFactory: https://explorer.kardiachain.io/token/0xA71FEcbdF6aDEfFEeed7b9884AC9E1c15346086c
UniswapRouter: https://explorer.kardiachain.io/token/0x70B0FC0a153F19F6369AE2A23e8133612526c8F0
WKAI: https://explorer.kardiachain.io/token/0xAF984E23EAA3E7967F3C5E007fbe397D8566D23d

Hoang Phan, [28 Jun 2021 at 18:18:45]:
...
const supply = await callHelpers(defilyContract, address.defily, 'totalSupply')

cái này là ví dụ call method nha a

export const sendTransactionToExtension = async (account, txData, toAddress) => {
const kardiaTransaction = kardiaClient.transaction

return kardiaTransaction.sendTransactionToExtension(
{
from: account,
gasPrice: await getRecommendedGasPrice(),
gas: GAS_LIMIT_DEFAULT,
data: txData,
to: toAddress,
},
true,
)
}

export const approve = async (contract, tokenAddress, account) => {
const txData = contract.invokeContract('approve', [address.masterChef, UINT256_MAX]).txData()

const response = await sendTransactionToExtension(account, txData, tokenAddress)

return response.transactionHash
}

cái này là ví dụ tương tác vs extension của kai

export const getRecommendedGasPrice = async () => {
const gasPrice = await kardiaClient.kaiChain.getGasPrice()
return Number(gasPrice)
}
