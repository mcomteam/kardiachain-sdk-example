import React from "react";

import Balance from "./components/Balance";
import { Contract } from "./components/Contract";
import KAI from "./components/KAI";
import KRC20 from "./components/KRC20";
import Transcation from "./components/Transaction";
import Wallet from "./components/Wallet";
import { HomeProvider, useHomeContext } from "./providers/HomeProvider";

export default function HomePage() {
  return (
    <>
      <HomeProvider>
        <ConfigWallet />
        <Balance />
        <Wallet />
        <KAI />
        <KRC20 />
        <Transcation />
        <Contract />
      </HomeProvider>
    </>
  );
}

function ConfigWallet() {
  const { walletAddress, setWalletAddress } = useHomeContext();
  return (
    <>
      <div className="flex flex-row space-x-4 items-center">
        <div>WALLET ADDRESS</div>
        <input
          type="text"
          placeholder="0x...."
          value={walletAddress}
          onChange={(val) => setWalletAddress!(val.target.value)}
        />
      </div>
    </>
  );
}
