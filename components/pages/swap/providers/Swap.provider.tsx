import { createContext, useContext, useEffect, useState } from "react";
import Web3 from "web3";
type SwapContextProps = {
  [x: string]: any;
  walletAddress?: string;
  setWalletAddress?: (address: string) => any;
  bscClient?: Web3;
  balance?: string;
  getBalance?: () => any;
};
export const SwapContext = createContext<SwapContextProps>({});

export function SwapProvider(props: any) {
  let [walletAddress, setWalletAddress] = useState<string>("");
  let [extensionData, setExtensionData] = useState<any>({});
  let [balance, setBalance] = useState<string>();
  const bscClient = new Web3(
    new Web3.providers.HttpProvider(
      "https://data-seed-prebsc-1-s1.binance.org:8545"
    )
  );
  let web3: Web3;
  const getBalance = async () => {
    console.log("walletAddress", walletAddress);
    const balance = await web3.eth.getBalance(walletAddress);
    setBalance(balance);
    return balance;
  };
  const context: SwapContextProps = {
    walletAddress,
    extensionData,
    bscClient,
    balance,
    getBalance,
  };
  let ethereum: any;
  if (typeof window != "undefined") {
    ethereum = (window as any).ethereum;
  }

  const isExtensionEnabled = () => {
    if (ethereum) {
      web3 = new Web3(ethereum);
      if (ethereum.isMetaMask) {
        ethereum.request({ method: "eth_requestAccounts" });
        return true;
      }
    }
    return false;
  };
  useEffect(() => {
    if (!isExtensionEnabled()) {
      setExtensionData({
        ...extensionData,
        extension: "KardiaChain Wallet Extension not found",
      });
    } else {
      const isKaiWallet = ethereum.isKaiWallet;
      const isConnected = ethereum.isConnected();
      ethereum
        .request({ method: "eth_requestAccounts" })
        .then((accounts: string[]) => {
          extensionData = {
            ...extensionData,
            extension: "KardiaChain Wallet Extension Enalbed",
            isConnected,
            isKaiWallet,
            accounts,
          };
          setExtensionData(extensionData);
          setWalletAddress!(ethereum.selectedAddress);
        });
      ethereum.on("accountsChanged", (accounts: string[]) => {
        extensionData = { ...extensionData, accounts };
        setExtensionData(extensionData);
        setWalletAddress!(ethereum.selectedAddress);
        getBalance();
      });
      ethereum.on("disconnect", (error: Error) => {
        extensionData = {
          ...extensionData,
          extension: error.message,
          isConnected: ethereum.isConnected(),
        };
        setExtensionData(extensionData);
      });
      ethereum.on("chainChanged", (chainId: string) => {
        console.log("chainChanged", chainId);
        extensionData = {
          ...extensionData,
          chainId: chainId,
        };
        setExtensionData(extensionData);
      });
      ethereum.on("networkChanged", (networkId: string) => {
        console.log("networkChanged", networkId);
        extensionData = {
          ...extensionData,
          networkId: networkId,
        };
        setExtensionData(extensionData);
      });
      interface ProviderMessage {
        type: string;
        data: unknown;
      }
      ethereum.on("message", (message: ProviderMessage) => {
        console.log("message", message);
        const messages = extensionData.messages || [];
        messages.push(message);
        extensionData = { ...extensionData, messages };
        setExtensionData(extensionData);
      });
    }
  }, []);
  return (
    <SwapContext.Provider value={context}>
      {props.children}
    </SwapContext.Provider>
  );
}

export const useSwapContext = () => useContext(SwapContext);
