import { kardiaClient } from "../../../../libs/kardia-client";
import uniswapRouterV2ABI from "../../../../abi/uniswapRouterV2.abi.json";
import { useEffect, useState } from "react";

export function Contract() {
  let [data, setData] = useState({});
  const RouterV2ContractAddress = "0x70B0FC0a153F19F6369AE2A23e8133612526c8F0";
  useEffect(() => {
    kardiaClient.contract.updateAbi(uniswapRouterV2ABI.abi);
    getWETH();
    kardiaClient.contract
      .invokeContract("factory", [])
      .call(RouterV2ContractAddress)
      .then((address) => {
        data = { ...data, Factory: address };
        setData(data);
      });
  }, []);
  return (
    <>
      <h1>Smart Contract Uniswap Router V2</h1>
      <pre>{JSON.stringify(data, null, 2)}</pre>
    </>
  );

  function getWETH() {
    kardiaClient.contract
      .invokeContract("WETH", [])
      .call(RouterV2ContractAddress)
      .then((address) => {
        data = { ...data, WKAI: address };
        setData(data);
      });
  }
}
