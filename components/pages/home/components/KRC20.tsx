import { useState } from "react";
import { kardiaClient } from "../../../../libs/kardia-client";
import { Button } from "../../../shared/Button";
import { useHomeContext } from "../providers/HomeProvider";

export default function KRC20() {
  const { walletAddress } = useHomeContext();
  const [balance, setBalance] = useState({});
  const [token, setToken] = useState(
    "0xD675fF2B0ff139E14F86D87b7a6049ca7C66d76e"
  );
  const [receiver, setReceiver] = useState("");
  const [transferAmount, setTransferAmount] = useState(0);
  const [transferRes, setTransferRes] = useState({});
  const getBalance = async () => {
    await kardiaClient.krc20.getFromAddress(token);
    const balance = await kardiaClient.krc20.balanceOf(walletAddress!);
    const decimals = await kardiaClient.krc20.getDecimals();
    const parsedBalance = balance / 10 ** decimals;
    console.log("balance", balance);
    setBalance(parsedBalance);
  };
  const transfer = async () => {
    if (receiver == "") {
      alert("Chưa nhập địa chỉ người cần chuyển.");
      return;
    }
    await kardiaClient.krc20.getFromAddress(token);
    const privateKey = prompt("Private Key", "");
    if (!privateKey || privateKey == "") return;
    const txResult = await kardiaClient.krc20.transfer(
      privateKey,
      receiver,
      transferAmount // Amount of tokens to send
    );
    console.log("transRes", txResult);
    setTransferRes(txResult);
  };
  return (
    <>
      <h1>KRC20 Module</h1>
      <div className="flex flex-row space-x-4 items-center">
        <div>KRC20_TOKEN_ADDRESS</div>
        <input
          type="text"
          placeholder="0x...."
          value={token}
          onChange={(val) => setToken(val.target.value)}
        />

        <Button text="Get Balance" onClick={getBalance} />
      </div>
      <pre>
        Balance:
        {JSON.stringify(balance, null, 2)}
      </pre>
      <h1>Transfer Token</h1>
      <div className="flex flex-row space-x-4 items-center">
        <div>RECEIVER_WALLET_ADDRESS</div>
        <input
          type="text"
          placeholder="0x...."
          value={receiver}
          onChange={(val) => setReceiver(val.target.value)}
        />
        <input
          type="number"
          placeholder="Nhấp số token càn chuyển"
          onChange={(val) => setTransferAmount(parseFloat(val.target.value))}
        />
        <Button text="Transfer" onClick={transfer} />
      </div>
      <pre>{JSON.stringify(transferRes, null, 2)}</pre>
    </>
  );
}
