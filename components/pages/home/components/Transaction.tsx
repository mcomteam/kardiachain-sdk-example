import { useState } from "react";
import { kardiaClient } from "../../../../libs/kardia-client";
import { Button } from "../../../shared/Button";
import { useHomeContext } from "../providers/HomeProvider";

export default function Transcation() {
  const { walletAddress } = useHomeContext();
  const [nonce, setNonce] = useState("");
  const [receiver, setReceiver] = useState("");
  const [transferAmount, setTransferAmount] = useState(0);
  const [transferRes, setTransferRes] = useState({});
  const [transHash, setTransHash] = useState("");
  const getNonce = async () => {
    const nonce = await kardiaClient.account.getNonce(walletAddress!);
    console.log("getNonce", nonce);
    setNonce(nonce);
    return nonce;
  };
  const transfer = async () => {
    if (receiver == "") {
      alert("Chưa nhập địa chỉ người nhận");
      return;
    }
    const nonce = await getNonce();
    const txData = {
      to: receiver,
      nonce,
      gas: 3000000, // Gas limit
      gasPrice: 1 * 10 ** 9, // Minimum Gas Price = 1 OXY
      value: transferAmount,
    };
    const privateKey = prompt("Private Key", "");
    if (!privateKey || privateKey == "") return;
    // Send transaction to network and get transaction hash immediately
    setTransferRes("Transaction Pending....");
    const txResult = await kardiaClient.transaction.sendTransaction(
      txData,
      privateKey,
      true, // Flag to indicate if you want to wait for the transaction to complete
      50000 // Time (in ms) you want to wait for transaction to complete, default will be 300000 (300s)
    );
    console.log("txHash", txResult);
    setTransferRes(txResult);
  };
  const transferByWallet = async () => {
    if (receiver == "") {
      alert("Chưa nhập địa chỉ người nhận");
      return;
    }
    const nonce = await getNonce();
    const txData = {
      to: receiver,
      nonce,
      gas: 3000000, // Gas limit
      gasPrice: 1 * 10 ** 9, // Minimum Gas Price = 1 OXY
      value: transferAmount,
    };
    // Send transaction to network and get transaction hash immediately
    setTransferRes("Transaction Pending....");
    debugger;
    const txResult = await kardiaClient.transaction.sendTransactionToExtension(
      txData,
      true, // Flag to indicate if you want to wait for the transaction to complete
      50000 // Time (in ms) you want to wait for transaction to complete, default will be 300000 (300s)
    );
    console.log("txHash", txResult);
    setTransferRes(txResult);
  };
  const fetchTransaction = async () => {
    const txDetail = await kardiaClient.transaction.getTransaction(transHash);
    setTransferRes(txDetail);
  };
  const fetchTransactionReceipt = async () => {
    const txReceipt = await kardiaClient.transaction.getTransactionReceipt(
      transHash
    );
    setTransferRes(txReceipt);
  };
  const clear = () => {
    setTransferRes({});
  };
  return (
    <>
      <h1>Transaction Module</h1>
      <div className="flex flex-row space-x-4 items-center">
        <Button text="Get Nonce" onClick={getNonce} />
        <div>
          Nonce: <b>{nonce}</b>
        </div>
      </div>
      <h1>Transfer By Transaction</h1>
      <div className="flex flex-row space-x-4 items-center">
        <div>RECEIVER_WALLET_ADDRESS</div>
        <input
          type="text"
          placeholder="0x...."
          value={receiver}
          onChange={(val) => setReceiver(val.target.value)}
        />
        <input
          type="number"
          placeholder="Nhấp số token càn chuyển"
          onChange={(val) => setTransferAmount(parseFloat(val.target.value))}
        />
        <Button text="Transfer" onClick={transfer} />
        <Button text="Transfer By Wallet" onClick={transferByWallet} />
      </div>
      <div className="flex flex-row space-x-4 items-center">
        <div>FETCH TRANSACTION DETAIL</div>
        <input
          type="text"
          placeholder="Transaction Hash..."
          value={transHash}
          onChange={(val) => setTransHash(val.target.value)}
        />
        <Button text="Fetch" onClick={fetchTransaction} />
        <Button text="Fetch Reciept" onClick={fetchTransactionReceipt} />
        <Button text="Clear" onClick={clear} />
      </div>

      <pre>{JSON.stringify(transferRes, null, 2)}</pre>
    </>
  );
}
