import { useState, useEffect } from "react";
import { kardiaClient } from "../../../../libs/kardia-client";
import { useHomeContext } from "../providers/HomeProvider";
import { Button } from "../../../shared/Button";
export default function Balance() {
  const { walletAddress: ADDRESS } = useHomeContext();
  const [data, setData] = useState({});
  const getBalance = () => {
    const BLOCK_HASH =
      "0x873daed41129cc9341def4cece3270e5faa2d3c9af270bf4bc49bd461b1d3d62";
    Promise.all([
      kardiaClient.account.getBalance(ADDRESS!),
      kardiaClient.account.getBalance(ADDRESS!, { blockHeight: 1 }),
      kardiaClient.account.getBalance(ADDRESS!, { blockHash: BLOCK_HASH }),
    ]).then(([latestBalance, balanceAtHeight, balanceAtHash]) =>
      setData({
        latestBalance,
        balanceAtHeight,
        balanceAtHash,
      })
    );
  };

  return (
    <div>
      <h1>Balance</h1>
      <Button text="Get Balance" onClick={getBalance} />
      <pre>{JSON.stringify(data, null, 2)}</pre>
    </div>
  );
}
