import Wallet from "./components/Wallet";
import { SwapProvider } from "./providers/Swap.provider";

export default function SwapPage(props: any) {
  return (
    <>
      <SwapProvider>
        <Wallet></Wallet>
      </SwapProvider>
    </>
  );
}
