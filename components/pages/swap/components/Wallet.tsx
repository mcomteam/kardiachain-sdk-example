import { useSwapContext } from "../providers/Swap.provider";

export default function Wallet() {
  const { walletAddress, balance } = useSwapContext();
  return (
    <>
      <h1>Wallet</h1>
      <p>Address: {walletAddress}</p>
      <p>Balance: {balance}</p>
    </>
  );
}
