import { kardiaClient } from "../../../../libs/kardia-client";
import { Button } from "../../../shared/Button";
import keccak256 from "keccak256";
import { useState } from "react";

export default function KAI() {
  const [data, setData] = useState({});
  const getFilterLogs = async () => {
    const filterId = await getFilter();
    const allLogs = await kardiaClient.kaiChain.getFilterLogs(filterId);
    console.log("getFilterLogs", allLogs);
    setData(allLogs);
  };
  const getFilterChanges = async () => {
    const filterId = await getFilter();
    const logs = await kardiaClient.kaiChain.getFilterChanges(filterId);
    console.log("getFilterChanges", logs);
    setData(logs);
  };
  const clear = () => {
    setData({});
  };
  return (
    <>
      <h1>KAI Module</h1>
      <div className="flex flex-row space-x-5">
        <Button text="getFilterLogs" onClick={getFilterLogs}></Button>
        <Button text="getFilterChanges" onClick={getFilterChanges}></Button>
        <Button text="clear" onClick={clear}></Button>
      </div>
      <pre>{JSON.stringify(data, null, 2)}</pre>
    </>
  );
}

async function getFilter() {
  const topic =
    "0x" + keccak256("Transfer(address,address,uint256)").toString("hex");
  const filterId = await kardiaClient.kaiChain.newFilter({
    fromBlock: 200000,
    topics: [topic],
  });
  return filterId;
}
