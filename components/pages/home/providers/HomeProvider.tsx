import { createContext, useContext, useState } from "react";
type HomeContextProps = {
  [x: string]: any;
  walletAddress?: string;
  setWalletAddress?: (address: string) => any;
};
export const HomeContext = createContext<HomeContextProps>({});

export function HomeProvider(props: any) {
  const [walletAddress, setWalletAddress] = useState(
    "0x46a83c43Ae220A227fCE193C28CF43C3C602b2b3"
  );
  const context: HomeContextProps = {
    walletAddress,
    setWalletAddress,
  };
  return (
    <HomeContext.Provider value={context}>
      {props.children}
    </HomeContext.Provider>
  );
}

export const useHomeContext = () => useContext(HomeContext);
